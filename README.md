
# Remote Wipe feature design notes

## Roles:
- **wipee** - the person who will have their account wiped.
- **wiper** - one (of at least 2) people who may activate the wipe.

## Message types:
- `setup` - sent from the wipee to each of their chosen wipers to establish them as wipers.
- `wipe` - sent from the wiper to the wipee to activate the wiping.
- `revoke` - sent from the wipee to tell a wiper that they are no longer a wiper - this is additional functionality and will be not be implemented in the initial round of development.
- `confirm` - sent from the wipee the wipers to inform them that the wipe was successfully activated. This will also not be implemented in the first round and it is debatable whether it useful or improves security.

## Events:
- `RemoteWipeActivatedEvent` - sent when a critical number of valid `wipe` messages are received.  This signs out of Briar, deletes all the application data, and removes it from the list of recently used apps. 

## Process:

- The wipee chooses a minimum of two wipers from their contacts list, sends them each a `setup` message, and also stores the messages locally.
- On receiving a `setup` message, wipers have an option enabled in the UI to activate the wipe - which sends a `wipe` message to the contact.
- On receiving a `wipe` message:
  - If the author of the message is not a member of the set of wipers, the message is ignored.
  - Otherwise, their author id and message timestamp is added to a set of received `wipe` messages.
  - Iterate over the set of received wipe messages and remove all entries with timestamps older than a certain limit (set at 24 hours ago) - effectively meaning a wipe is 'cancelled' if a day passes between the messages being received.
  - If the size of the set of messages is greater than or equal to the threshold, the remote wipe event is activated. As additional functionality, a confirmation message could also be sent to the wipers who sent wipe messages.

Proposal - we use a fixed threshold of two rather than allowing the user to choose. This decision does not effect the back-end design, so we can re-consider it later. 

## UI

- An option to setup remote wipe, choose the wipers and confirm that it is setup correctly.
- A screen which displays the current list of wipers, and ideally an option to revoke either individual wipers or everyone at once.
- A notification that you have been appointed as a wiper (or that you are no longer appointed as a wiper).
- An option to activate the wipe, with an explainer screen describing the implications.
- What happens in the UI when a `wipe` message is received? Current proposal - absolutely nothing visible in UI - as it may cause suspicion.

